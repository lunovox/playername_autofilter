

-- Function to count the number of consonants, vowels, numbers, underlines, and invalid characters in a string
modNormaName.countCharacters = function(text)
	local chars = 0
	local numbers = 0
	local underlines = 0
	local vowels = 0
	local consonants = 0
	local invalids = 0

	-- Iterate over each character in the text
	for i = 1, text:len() do
	   local character = text:sub(i, i)
		if type(character:match("[0-9]"))~="nil" then
			numbers = numbers + 1
		elseif type(character:match("_"))~="nil" then
			underlines = underlines + 1
		elseif type(character:match("[A-Za-z]"))~="nil"
		   and type(character:match("[aeiyouAEIYOU]"))~="nil" 
	   then
			vowels = vowels + 1
		elseif type(character:match("[A-Za-z]"))~="nil"
		   and type(character:match("[aeiyouAEIYOU]"))=="nil" 
		then
			consonants = consonants + 1
		else
			invalids = invalids + 1
		end
	   chars = chars + 1
	end

	-- Return a dictionary with the counts
	return {
		chars = chars,
		numbers = numbers,
		underlines = underlines,
		vowels = vowels,
		consonants = consonants,
		invalids = invalids,
	}
end

modNormaName.on_prejoinplayer = function(playername, playerip)
	local support_message = minetest.settings:get("modNormaName.support_message") or modNormaName.translate("If you need support, please contact the administrator of this server!")
	local isAppropriate, txtReason = modNormaName.is_appropriate_playername(playername)
   if not isAppropriate then
      --minetest.disconnect_player(playername, " "..txtReason.."\n"..support_message)
      return txtReason.."\n"..support_message
   end
end

modNormaName.on_joinplayer = function(player)
   local playername = player:get_player_name()
   local isAppropriate, txtReason = modNormaName.is_appropriate_playername(playername)
   if not isAppropriate then
      local support_message = minetest.settings:get("modNormaName.support_message") or modNormaName.translate("If you need support, please contact the administrator of this server!")
      minetest.disconnect_player(playername, " "..txtReason.."\n"..support_message)
   end
   return isAppropriate
end

modNormaName.is_appropriate_playername = function(playername)
   local counts = modNormaName.countCharacters(playername)
   
   -- Verifica se possuem consoantes
   if counts.consonants < 1 then
      return false, modNormaName.translate("The playername does not have consonants.")
   end
   
   -- Verifica se possuem pelo menos uma vogal
   if counts.vowels < 1 then
      return false, modNormaName.translate("The playername must have at least one vowel.")
   end
   
   -- Verifica se não há mais de dois números consecutivos 
   if type(playername:match("%d%d%d"))~="nil" then --tested
      return false, modNormaName.translate("The playername cannot have more than two consecutive numeric digits.")
   end
   
   -- Verifica se possuem pelo menos dois numrros.
   if counts.numbers > 3 then
      return false, modNormaName.translate("The playername cannot have more than three numeric digits anywhere.")
   end
   
   -- Verifica se não possuir caracteres especiais além do underline.
   if counts.invalids >= 1 then
      return false, modNormaName.translate("The playername cannot have special characters other than the underlines.")
   end
   
   -- Verifica se não há mais de um underline consecutivos 
   if type(playername:match("__+"))~="nil" then --tested
      return false, modNormaName.translate("The playername cannot have more than one consecutive underline.")
   end
   
   return true, "ok"
end




