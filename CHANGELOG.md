-----------------------------------------------------------------------
# NORMAL NAME MOD
-----------------------------------------------------------------------

## NORMALNAME v0.1.0 alpha : early access version

Block the player from joining the server if:

* [x] The playername does not have consonants.
* [x] The playername must have at least two vowels.
* [x] The playername cannot have more than two consecutive numeric digits.
* [x] The playername cannot have more than three numeric digits anywhere.
* [x] The playername cannot have special characters other than the underlines.
* [x] The playername cannot have more than one consecutive underline.
* [ ] The playername blocked not is part of the 'whitelist'.
* [ ] The playername is part of the 'blacklist', even if it is also part of the 'whitelist'.

-----------------------------------------------------------------------
