----------------------------------------------
# HOW TO TRANSLATE
----------------------------------------------

* Created: 2024-04-02
* Update: 2024-04-04

## **Step 01** : To generate an updated file '**template.pot**':

Sintaxe:
````bash
cd <this_mod_path>
xgettext -n *.lua -L Lua \
--from-code=UTF-8 \
--force-po \
--keyword=modNormaName.translate \
-o ./locale/template.pot
````

----------------------------------------------

## **Step 02** : To select your native language by terminal command:

List of native language code used: 

| Language	| Region	| Code |
| :-------: | :----: | :--: |
| English | United States<br/>(**Default of this Mod**) | `en` |
| Bengali | Bangladesh | `bn_BD` |
| Danish | Denmark | `da` |
| German | Germany | `de` |
| Greek | Greece | `el` |
| Spanish | Spain | `es` |
| French | France | `fr` |
| Hungarian | Hungary | `hu` |
| Italian | Italy | `it` |
| Japanese | Japan | `ja` |
| Korean | Korea | `ko` |
| Mongolian | Mongolia | `mn` |
| Dutch | Netherlands | `nl` |
| Polish | Poland | `pl` |
| Portuguese | Portugual | `pt` |
| Brazilian Portuguese | Brazil | `pt_BR` |
| Russian | Russia | `ru` |
| Turkish | Turkey | `tr` |
| Chinese | China | `zh_CN` |
| Chinese | Taiwan | `zh_TW` |

Others native language code used: 

> ca;cs;da;de;dv;eo;es;et;fr;hu;id;it;ja;jbo;kn;lt;ms;nb;nl;pl;pt;pt_BR;ro;ru;sl;sr_Cyrl;sv;sw;tr;uk

Sintaxe:
```bash
translate_to="<your_native_language_code>"
```

Use the bash/shell terminal command:
```bash
# Example only to portuguese language (pt):
translate_to="pt"
```

----------------------------------------------

## **Step 03** : Create '.po' file from a 'template.pot' file if '.po' file does not exist.

Use the bash/shell terminal command:

```bash
cd ./locale/
# only gerate if file not exist.
if [ ! -f "${translate_to}.po" ]; then 
   msginit --no-translator --no-wrap \
   --locale="${translate_to}.UTF-8" \
   --output-file="./${translate_to}.po" \
   --input=./template.pot
fi
```
----------------------------------------------

## **Step 04** : Update '.po' file from an updated 'template.pot' file.

Use the bash/shell terminal command:

```bash
msgmerge --sort-output --no-wrap \
--update --backup=off \
./${translate_to}.po ./template.pot
```
----------------------------------------------

## **Step 05** : To edit '**.po**' files:

Use the graphics application 'poedit' when writing the terminal command:

```bash
sudo apt install poedit
poedit "./${translate_to}.po"
```

If you do not have access to the graphical interface, use the terminal application 'tilde' when writing the terminal command:

```bash
sudo apt install tilde
tilde "./${translate_to}.po"
```

Example of translation from standard language of this mod to Portuguese:

```
msgid "Hello World!"
msgstr "Olá Mundo!"
```
**Note:** The sequence `@n` will be replaced by a line break (an 'enter').

----------------------------------------------

## **Step 06** : Convert '.po' file to '.tr' file.

Use the bash/shell terminal command:

```bash
lua po2tr.lua "normalname" \
"./${translate_to}.po"
mv "./${translate_to}.tr" \
"./normalname.${translate_to}.tr"
```
----------------------------------------------

## **Step 07** : to enable your language in minetest

To enable the translate to your language, write `language = <your_native_language_code>` in  `minetest.conf` file. Or write the command `/set -n language <your_native_language_code>` in minetest game chat, and run again the minetest game.

----------------------------------------------

## **Step 08** : Submit yout translate

Please submit your "`<your_native_language_code>.po`" translation files via [merge requests], or you can email it to [<lunovox@disroot.org>].

### 😁👍 We appreciate your collaboration!

----------------------------------------------


<!--
> See more: 
	* https://forum.minetest.net/viewtopic.php?f=47&t=21974
	* https://github.com/minetest/minetest/issues/8158
	* https://gist.githubusercontent.com/mamchenkov/3690981/raw/8ebd48c2af20c893c164e8d5245d9450ad682104/update_translations.sh
	* https://gitlab.com/4w/xtend/-/blob/master/xtend_default/tools/convert_po_file_to_tr_file/convert_po_file_to_tr_file.lua
-->

[merge requests]:https://gitlab.com/lunovox/normalname/-/merge_requests
[<lunovox@disroot.org>]:mailto:lunovox@disroot.org?subject=My%20translation%20of%20the%20Postal%20Sys%20mod.