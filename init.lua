modNormaName = {
   modname = minetest.get_current_modname(),
   modpath = minetest.get_modpath(minetest.get_current_modname()),
}

dofile(modNormaName.modpath.."/translate.lua")
dofile(modNormaName.modpath.."/api.lua")

minetest.register_on_newplayer(function(player)
   return modNormaName.on_joinplayer(player)
end)

minetest.register_on_prejoinplayer(function(playername, playerip)
	return modNormaName.on_prejoinplayer(playername, playerip)
end)

minetest.register_on_joinplayer(function(player, last_login)
   return modNormaName.on_joinplayer(player)
end)

minetest.log('action',"[MOD] "..modNormaName.modname:upper().." loaded!")
