if type(minetest.get_translator) ~= "nil" 
   and type(minetest.get_current_modname) ~= "nil" 
   and type(minetest.get_modpath) ~= "nil" 
   and minetest.get_modpath(minetest.get_current_modname()) 
then
	modNormaName.translate = minetest.get_translator(minetest.get_current_modname())
else
	modNormaName.translate = function(s) return s end
end
