![favicon]

Screenshots: [01]  [02]  [03]

# [NORMAL NAME]

[![minetest_icon]][minetest_link] Block new players and old players that who have a playername too irregular for a normal human.

### Block the player from joining the server if:
 
* [x] The playername does not have consonants.
* [x] The playername must have at least one vowel. (Character "y" will also be considered as a vowel similar to "i".)
* [x] The playername cannot have more than two consecutive numeric digits.
* [x] The playername cannot have more than three numeric digits anywhere.
* [x] The playername cannot have special characters other than the underlines.
* [x] The playername cannot have more than one consecutive underline.

## Tasks performed in future of this mod:
 
* [ ] Block the player from joining the server if: The playername blocked not is part of the 'whitelist'.
* [ ] Block the player from joining the server if: The playername is part of the 'blacklist', even if it is also part of the 'whitelist'.

## 📦 **Dependencies:**

There is no dependency on other mods, but there is a dependency on the "[Minetest Game]" on version 5.7 or higher.

## 📜 **License:**

* [![license_icon]][license_link] This program is free software; you can redistribute it and/or modify it under the terms of the [GNU Affero General Public License][license_link] as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. 

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

## ️🔬 **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](https://qoto.org/@lunovox), [WebChat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [audio conference](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

## 📌 **Change Log:**

* To find out what changed, see the [CHANGELOG.md] file!

## 💾 **Downloads:**

* [Stable Versions] : There are several versions in different stages of completion.  Usually recommended for servers because they are more secure.

* [Minetest Content] : You can download a stable version of this mod using the Minetest engine itself.  You just need to run Minetest itself, search for the word 'normalname', and click the 'download' button.

* [Git Repository] : It is the version currently under construction.  There may be some stability if used on servers.  It is only recommended for testers and mod developers.

## 🌍 **Internacionalization:**

### **Available Languages:**

* 🇬🇧 Generic English [Default, Concluded: 100%]
* 🇵🇹 Português Genérico (Concluded: 100%)
* 🇧🇷 Português Brasileiro (Concluded: 100%)

  **Translate this mod to your Language:** See more details in file '[locale/README.md]'.

## 🌈 **Special Privileges:**

| Privilege | Descryption |
| :--: | :-- |
| ````server````		| 🙋 : Allows you to edit the 'whitelist' and 'blacklist' of mod 'normalname' at server runtime. |

## ⚙️ **Settings:**

In **minetest.conf** file:

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values in case you want to directly change the ````minetest.conf```` file.

| Settings | Descryption |
| :-- | :-- |
| ```modNormaName.debug = <boolean>```		| If show debug info of this mod. Only util to developers. Default: ```false```. | 
| ```modNormaName.support_message = <number>``` | If the playername is blocked, this is the message that will appear on the player's screen explaining ways to contact the server administration to request support. Default: ```If you need support, please contact the administrator of this server!```. Sample: ```If you need support, please contact us by mail <foo@bar>!``` |   



[favicon]:https://gitlab.com/lunovox/normalname/-/raw/main/favicon.png
[01]:https://gitlab.com/lunovox/normalname/-/raw/main/screenshots/screenshot.1.png
[02]:https://gitlab.com/lunovox/normalname/-/raw/main/screenshots/screenshot.2.png
[03]:https://gitlab.com/lunovox/normalname/-/raw/main/screenshots/screenshot.3.png
[CHANGELOG.md]:https://gitlab.com/lunovox/normalname/-/blob/main/CHANGELOG.md
[Git Repository]:https://gitlab.com/lunovox/normalname
[license_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=Download&color=yellow
[license_link]:https://gitlab.com/lunovox/normalname/-/raw/main/LICENSE
[locale/README.md]:https://gitlab.com/lunovox/normalname/-/tree/main/locale?ref_type=heads
[NORMAL NAME]:https://gitlab.com/lunovox/normalname
[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Mod&color=brightgreen
[minetest_link]:https://minetest.net
[Minetest Content]:https://content.minetest.net/packages/Lunovox/normalname/
[Minetest Game]:https://content.minetest.net/packages/Minetest/minetest_game/
[Stable Versions]:https://gitlab.com/lunovox/normalname/-/tags
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

